# function to require packages with suppressed warnings
requireSup <- function(required.packages){
  if (DEBUG){
    lapply(required.packages, require, character.only=TRUE)
  } else {
    suppressMessages(tmp <- lapply(required.packages, require, character.only=TRUE))
  }
}

# function to suppress ggsave messages
ggsaveSup <- function(...){
  if (DEBUG){
    ggsave(...)
  } else {
    suppressMessages(ggsave(...))
  }
}
