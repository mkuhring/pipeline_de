# Differential Expression Pipeline

The Differential Expression Pipeline mainly comprises mapping of RNA-Seq reads
(TopHat), read counting per gene (Subread's featureCount), differential
expression analysis (DESeq2) as well as mapping quality control (QualiMap).
The pipeline currently supports simple pairwise differential expression based
on one condition or a time series model tested against a baseline model over
several time points.

Other optional workflow implementations using e.g. Cuffdiff or Kallisto with
Sleuth for mapping and/or differential analysis are experimental/incomplete and
thus not recommended to use.


## Configuration and Execution

The pipeline is executed by calling Snakemake within the project folder
(working directory, configured paths are relative to here and all results and
intermediate data will be dropped here):

    snakemake -s path/to/pipeline/Snakefile --configfile snakemake.config.json --cores 4 -pk

The workflow is configured with a json file. One of the main parameters in
the configuration is the tab separated sample file (indicated via
`sample_file`), which must contain the following columns (with headers as
named here), with one row per sample each:

* `sample`: name of the sample
* `group`: a group name, used to group samples for joint analysis and to assign
references
* `condition=*`: expression of a condition for the sample (with \* being the
condition's name, e.g. treatment, time, batch, replicate, ...)
* `folder`: the directory with the read files
* `pair1`: the first pairs of paired fastq files (comma separated)
* `pair2:`: the second pairs of paired fastq files (comma separated, same
order as pair1!)
* `unpaired`: any unpaired reads fastq files (comma separated)

The condition column may occur multiple times. However, only the first condition
column is currently considered for pairwise differential expression analysis
within a group. Alternatively, the "time" condition is used if `de_mode` is set
to "timeseries". More complex designs and contrasts are not yet implemented,
though additional conditions can be used for batch correction.

Further required parameters include a project description (`project_name`) and
one or more reference sets per group (`references`), each including a
reference genome sequence (`genome`, as fasta/fa/fna) and corresponding
annotations (`annotation`, as gtf/gff/gff3). For each combination of group and
references, analysis will be executed separately. A minimum json
configuration may look like the following:

    {
    "project_name" : "Dual RNA-Seq",
    "sample_file" : "ascaris_main.txt",
    "references" : {"worm" : ["Asuum01"],
                    "pig"  : ["Sscrofa01", "Sscrofa02"]},
    "genomes" : {"Asuum01"   : "../references/Ascaris_suum_assembly.fasta",
                 "Sscrofa01" : "../references/Sus_scrofa.Sscrofa10.2.dna.toplevel.fa",
                 "Sscrofa02" : "../references/GCF_000003025.5_Sscrofa10.2_genomic.fna"},
    "annotations" : {"Asuum01"   : "../references/Ascaris_suum_genome.gff3",
                     "Sscrofa01" : "../references/Sus_scrofa.Sscrofa10.2.85.gtf",
                     "Sscrofa02" : "../references/GCF_000003025.5_Sscrofa10.2_genomic.gff"}
    }

For more configuration options and examples, please refer to
[scripts_snakemake/configuration.snakefile](scripts_snakemake/configuration.snakefile)
and [example/config.json](example/config.json).

The final differentially expressed genes (DEGs) can currently be found in
`deseq2/{test_level}/featureCounts/{group}/{reference}/de-{mode}/{design}/results_sig_*_with_fc.csv`,
along with all genes of the group (`results_all_*.csv`, including
reference and potentially novel genes) as well as common visualizations such
as MA plots, clustering heatmaps and PCAs. Reports on mapping quality control
are available in `qualimap/{group}/`.


## General Notes

* Sample names should never start with a digit, else they will raise errors in several R packages (mostly DE related).


## Requirements

Requirements may vary with applied parameters (and thus executed Snakemake
rules), but the following lists all possibly used tools and libraries.

Following tools must be available via the PATH environmental variable (tested version in brackets):
* Bowtie2 (2.2.9)
* Cufflinks (v2.2.1)
* Kallisto (v0.43.0)
* Python (2.7.9)
* Python3 (3.4.3)
* QualiMap (v.2.2)
* R (3.3.1)
* Snakemake (3.5.5)
* Subread (1.5.0-p3)
* TopHat (v2.1.1)

Following R packages are required:
* cummeRbund (Bioconductor)
* DESeq2 (Bioconductor)
* devtools
* ggdendro
* ggplot2
* grid
* gridExtra
* pheatmap
* RColorBrewer
* readr
* rhdf5 (Bioconductor)
* sleuth (GitHub)
* splines
* tximport (Bioconductor)

R packages can be automatically installed by executing "Rscript scripts_r/installion.packages.R" in the DE pipeline folder.


## Workflow overview

Example sequence of applied Snakemake rules:

![Rulegraph](rulegraph.png)


## Trouble-Shooting

* **DESeq2** visualization fail due to X11 issues:
  * Although pheatmaps is used in silent mode, it still tries to open empty plot windows via X11.
  * Try same workaround as for **QualiMap** and **Sleuth** X11 issues.
* **QualiMap** fails because of missing R packages:
  * QualiMap requires several are R packages which can be easily installed by using its accompanying install script. In the QualiMap folder run "Rscript scripts/installDependencies.r".
  * This may occur again after switching to a new or different R version since each version utilizes a separate package folder.
* **QualiMap bamqc** fails with the following error:
  * *java.lang.IllegalStateException: Inappropriate call if not paired read*
  * This problem seems to occur if a sample comprises both, paired and unpaired reads (which is probably always the case after trimming with QCumber).
  * As a workaround, don't run QualiMap with a strand-specific parameter (such as "--sequencing-protocol strand-specific-reverse").
  * Drawback: QualiMap won't check anymore for success rates of strand-specific mappings.
* **QualiMap** or **Sleuth** fail due to X11 issues:
  * Though the pipeline is completly command-line based, QualiMap and Sleuth (resp. the included plots) seem to query X11 at some point.
  * While this can be a problem with missing Cairo libaries in locally compiled R versions (in the case of Sleuth), it is often a problem with the DISPLAY variable when using tmux or screen.
  * As a workaround, don't run the pipeline within tmux or screen (or at least use a session with only one pane and windows and make sure X11 forwarding is really working there, e.g. by executing "firefox").
* **Sleuth** fails with following error message:
  * *Error in if (sum(valid) == 0) { : missing value where TRUE/FALSE needed*
  * Sleuth only works with replicates ([reference](https://groups.google.com/forum/#!topic/kallisto-sleuth-users/nLtzFHqUm2Y)).
  * Remove sleuth analysis from the DE_METHOD parameter, if no replicates are available.