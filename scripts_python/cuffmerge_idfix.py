# to be used via mapping.snakefile -> rule cuffmerge_idfix

# replace Cuffmerge gene_ids with reference gene_ids via gene_names (if available) in merged.gtf

from collections import OrderedDict

# read reference GTF IDs (gene_name -> gene_id)
ref_dict = dict()
for line in open(snakemake.input.ids, "r"):
    splits = line.strip('\n').split('\t')
    if splits[2] != "":
        ref_dict[splits[2].strip('\"')] = splits[1].strip('\"')

# iterate Cuffmerge GTF and replace ids
out_file = open(snakemake.output[0], "w")
for line in open(snakemake.input.gtf):
    if not line.startswith("#"):
        splits = line.split('\t')
        attr = OrderedDict(item.strip().split(' ') for item in splits[8].strip('\n').split(';') if item)
        if "gene_id" in attr and "gene_name" in attr:
            # replace gene_id with reference gene_id
            attr["gene_id"] = '\"' + ref_dict.get(attr["gene_name"].strip('\"'), attr["gene_name"].strip('\"')) + '\"'
        splits[8] = '; '.join([' '.join(a) for a in attr.items()]) + ";"
        out_file.write("\t".join(splits) + "\n")
    else:
        out_file.write(line)
out_file.close()