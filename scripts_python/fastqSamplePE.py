#!/usr/bin/env python
# author: Mathias Kuhring

# sample reads of paired fastq files to a given number (roughly)
# provide the approximate number (in millions! e.g. "10" for 10 million)
# of read pairs to obtain, the two input fastq files (left and right 
# reads must be in the same order) and two names for output files.

import itertools
import random
import sys

import HTSeq

print("counting reads...")
num_lines = sum(1 for _ in open(sys.argv[2]))
num_reads = int(num_lines / 4)

# based on: http://seqanswers.com/forums/showthread.php?t=12070
num_wanted = int(float(sys.argv[1]) * 1000000)
fraction = float(num_wanted) / float(num_reads)

print("total # reads:   " + str(num_reads))
print("target # reads:  " + str(num_wanted))
print("target fraction: " + str(fraction))

in1 = iter(HTSeq.FastqReader(sys.argv[2]))
in2 = iter(HTSeq.FastqReader(sys.argv[3]))
out1 = open(sys.argv[4], "w")
out2 = open(sys.argv[5], "w")

print("sampling reads...")

# sample by number
selection = sorted(random.sample(range(num_reads), num_wanted))
sel_idx = 0
read_num = 0
for read1, read2 in itertools.izip(in1, in2):
    if selection[sel_idx] == read_num:
        read1.write_to_fastq_file(out1)
        read2.write_to_fastq_file(out2)
        sel_idx += 1
    if sel_idx >= len(selection):
        break
    read_num += 1

# sample by fraction
for read1, read2 in itertools.izip(in1, in2):
    if random.random() < fraction:
        read1.write_to_fastq_file(out1)
        read2.write_to_fastq_file(out2)

out1.close()
out2.close()
