#!/usr/bin/env python

# extract transcript_id and corresponding gene_id (and gene_name if available)
# based on: https://www.biostars.org/p/140471/#140472

import sys

# features of interest
features = ["CDS", "exon", "transcript"]

# dict to uniquely collect transcript_id and corresponding gene_id
id_mapping = dict()

# iterate file
for line in sys.stdin:
    # skip comment lines
    if not line.startswith("#"):
        splits = line.split('\t')
        # check if line is feature of interest
        if (splits[2] in features):
            # parse attribute fields into dict
            attr = dict(item.strip().split(' ') for item in splits[8].strip('\n').split(';') if item)
            # store attributes of interest
            transcript_id = attr['transcript_id'].strip('\"')
            gene_id = attr['gene_id'].strip('\"')
            gene_name = attr.get('gene_name', "").strip('\"')
            id_mapping[transcript_id] = gene_id + '\t' + gene_name

# print each transcript_id and corresponding gene_id to std
for key, value in id_mapping.iteritems():
    print key + "\t" + value
