#! /usr/bin/python3

# Mail script
# By Tobias Loka and Wojtek Dabrowski

import sys
import argparse

# Import smtplib for the actual sending function
import smtplib

# Here are the email package modules we'll need
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def parse_args():
    parser = argparse.ArgumentParser(description="A small script to send mails via the shell. PLEASE USE PYTHON 3.0+!")
    parser.add_argument("To", help="E-Mail address of the recipients [separate by commas without whitespace]")
    parser.add_argument("-f", "--sender", help="Mail address of the sender")
    parser.add_argument("-s", "--subject", help="Subject of the mail [Set in quotes]", default="You got a mail")
    parser.add_argument("-a", "--attach",
                        help="List of attachments [separate by commas without whitespace; Both relative or abolute paths are permitted]")
    parser.add_argument("-t", "--text", help="Text content of the mail. [Set in quotes; Please do not use HTML]",
                        default="Hello world, this is a mail.")
    parser.add_argument("-S", "--smtp", help="Change SMTP server [default: smtp-connect.rki.local]",
                        default="smtp-connect.rki.local")

    args = parser.parse_args()

    return args


def main(argv):
    args = parse_args()

    COMMASPACE = ', '
    mailto = args.To.split(',')
    me = args.sender
    text = args.text
    files = ""
    if args.attach is not None:
        files = args.attach.split(',')
    subject = args.subject

    # Create the container (outer) email message.
    msg = MIMEMultipart()
    msg['Subject'] = subject

    msg['From'] = me
    msg['To'] = COMMASPACE.join(mailto)
    msg.preamble = subject

    for the_file in files:
        f = open(the_file)
        attachment = MIMEText(f.read())
        fns = the_file.rsplit('/', 1)
        fn = the_file.rsplit('/', 1)[len(fns) - 1]
        attachment.add_header('Content-Disposition', 'attachment', filename=fn)
        msg.attach(attachment)

    textPart = MIMEText(text, 'plain')
    msg.attach(textPart)

    # Send the email via our own SMTP server.
    s = smtplib.SMTP(args.smtp)
    s.send_message(msg)
    s.quit()


if __name__ == "__main__":
    main(sys.argv)
