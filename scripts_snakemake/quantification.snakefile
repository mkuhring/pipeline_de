### Quantification of mapped reads per feature ###

# count with featureCounts (from the Subread package)
# default feature = exon, summarized by gene_id
# TODO: indicate strand specificity?
rule featureCounts_count:
    version:
        subprocess.check_output("featureCounts -v 2>&1 >/dev/null | head -2 | tail -1 | cut -d \" \" -f2", shell=True)
    input:
        annotation = lambda wc: ANNOTATIONS_GTF[wc.reference] \
            if REF_ONLY else "cuffmerge/" +  wc.group + "/" + wc.reference + "/merged_fixed.gtf",
        mapped_reads = lambda wc: ["tophat/" + wc.reference + "_vs_" + sample + "/accepted_hits.bam" for sample in listGroupSamples(wc.group)]
    output:
        "counting/featureCounts/{group}/{reference}/counts.txt"
    benchmark:
        "logs/featureCounts/{group}/{reference}.txt"
    log:
        "logs/featureCounts/{group}/{reference}.log"
    threads:
        THREADS_MAX
    shell:
        "featureCounts -T {threads} -a {input.annotation} -o {output} {input.mapped_reads} 2>&1 | tee {log}"

# rename featureCounts colums and remove unused columns
rule featureCounts_clean:
    input:
        "counting/featureCounts/{group}/{reference}/counts.txt"
    output:
        "counting/featureCounts/{group}/{reference}/counts.clean.txt"
    params:
        new_header = lambda wc: "Geneid\t" + "\t".join(listGroupSamples(wc.group))
    shell:
        "echo {params.new_header} > {output} && tail -n +3 {input} | cut -f2-6 --complement >> {output}"

# summarize sample/replicate and condition info for a group
rule condition_summary_file:
    input:
        lambda wc: ["tophat/" + wc.reference + "_vs_" + sample + "/accepted_hits.bam" for sample in listGroupSamples(wc.group)]
    output:
        "infos/{group}/{reference}/de-{mode}/conditions.txt"
    run:
        wc = wildcards
        f = open(output[0],'w')
        f.write("sample" + "\t" + "\t".join(CONDITION_NAMES) + "\n")
        for sample in GROUP_SAMPLES[wc.group]:
            cond = "\t".join(SAMPLE_CONDITIONS[sample])
            line = sample + "\t" + cond + "\n"
            f.write(line)
        f.close()
