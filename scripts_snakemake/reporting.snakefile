### Pipeline versioning ###

# get version of snakemake workflow (aka git tag + commit hash)
# NOTE: make sure to commit latest workflow to get an updated version number!
rule pipeline_version_git:
    output:
        temp("reports/pipeline.version")
    version:
        str(subprocess.check_output("cd " + WORKFLOW_PATH +
        " && git describe --tags --long", shell=True), 'utf-8')
    run:
        file = open(output[0], "w")
        file.write(version)
        file.close()


### Snakemake logging ###
onsuccess:
    print("Workflow finished, no error")
    shell("mkdir -p reports && cat {log} > reports/" + EXEC_TIME + ".snakemake.success.log")
    if MAIL:
        shell(WORKFLOW_PATH + "/scripts_python/send_mail.py -f " + MAIL_FROM +
              " -s \"DE analysis of project '" + PROJECT + "': terminated successfully :-)\"" +
              " -t \"\" -a reports/" + EXEC_TIME + ".snakemake.success.log " + MAIL_TO)

onerror:
    print("An error occurred")
    shell("mkdir -p reports && cat {log} > reports/" + EXEC_TIME + ".snakemake.error.log")
    if MAIL:
        shell(WORKFLOW_PATH + "/scripts_python/send_mail.py -f " + MAIL_FROM +
              " -s \"DE analysis of project '" + PROJECT + "': terminated unsuccessfully :-(\"" +
              " -t \"\" -a reports/" + EXEC_TIME + ".snakemake.error.log " + MAIL_TO)


### Report creation ###

# test report rule
rule report:
    input:
        "reports/pipeline.version"
    output:
        "reports/" + EXEC_TIME + ".report.txt"
    shell:
        "echo \"DE Pipeline\" > {output} && echo \"Version:\" >> {output} && cat {input} >> {output}"

# TODO: gtf-filter: report removed elements (at least the number)
# TODO: summarize Mapping and QualiMap results
# TODO: collect Kallisto saturation results
