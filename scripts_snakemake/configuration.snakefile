### Configuration ###

# control additional output/warnings
DEBUGGING = config.get("debugging", False)

# global thread config for rules
THREADS_MAX = config.get("threads_max", 4)

# reporting
MAIL        = config.get("mail", False)
if MAIL:
    MAIL_FROM   = config["mail_from"]
    MAIL_TO     = config["mail_to"]

# workflow control
SATURATION_ON = False
# turned off completely, because not compatible with new sample input structure yet
# SATURATION_ON = config.get("saturation_on", True)
QUALIMAP_ON = config.get("qualimap_on", True)

# differential expression parameters
DE_MODE = config.get("de_mode", ["pairwise"])
DE_METHODS = config.get("de_methods", ["deseq2"])
DE_TEST = config.get("de_test", [0.01])
DE_TOP = min(config.get("de_top", 20), 20)

DESIGNS = config.get("designs", "design1")
BATCH_VARS = config.get("batch_effects", {"design1" : "none"})

TIME_SCALE = config.get("time_scale", "hour") if "timeseries" in DE_MODE else ""
TIME_PAIRS = config["time_foldchange_pairs"] if "timeseries" in DE_MODE else []

FC_THRESHOLDS = config.get("foldchange_thresholds", [1.5])

# project description
PROJECT = config["project_name"]

# genome and transcriptome references
REFERENCES = config["references"]
GENOMES = config["genomes"]
TRANSCRIPTS = config["transcripts"] if "kallisto" in DE_METHODS else []
ANNOTATIONS = config["annotations"]

GTF_FILTER = config.get("gtf_filter", True)

# TopHat
REF_ONLY = config.get("tophat_no-novel-juncs", False)

# QualiMap
QUALIMAP_PROTOCOL = config.get("qualimap_protocol", "")
QUALIMAP_REPORT = config.get("qualimap_report", "pdf")
QUALIMAP_MEMORY = "32G"

# Kallisto and saturation analysis
SLEUTH_FEATURE = config.get("sleuth_feature", ["transcripts"])
KALLISTO_NOVEL = config.get("kallisto_novel", False)
KALLISTO_BOOTSTRAPS = config.get("kallisto_bootstrap-samples", 50)

# not possible yet with current sample configuration
KALLISTO_FRACTIONS = config["kallisto_saturation_fractions"] if SATURATION_ON else []
for i, fraction in enumerate(KALLISTO_FRACTIONS):
    KALLISTO_FRACTIONS[i] = "_" + fraction if fraction else fraction

if KALLISTO_NOVEL and REF_ONLY:
    print("ERROR: kallisto_novel==true requires tophat_no-novel-juncs==false!")
    sys.exit(1)


# host filter method
HOST_FILTER_METHOD = config.get("host_filter_method", "bmtagger")
# host filter reference per group
HOST_FILTER_REF = config.get("host_filter_reference", dict())
# host filter per sample (just status, host path not needed)
HOST_FILTER_SAMPLES = set()
# host filter per fastq file (links to host path)
HOST_FILTER_FILES = dict()

### Sample Description ###

# sample description lists/dicts
GROUPS = list()
GROUP_SAMPLES = dict()
CONDITION_NAMES = list()
SAMPLE_CONDITIONS = dict()

# sample file dicts
SAMPLE_FOLDER = dict()
SAMPLE_P1 = dict()
SAMPLE_P2 = dict()
SAMPLE_UP = dict()

# parse description from file
SAMPLE_FILE = config["sample_file"]
with open(SAMPLE_FILE, 'r') as f:
    # get column positions
    header = f.readline().strip().split("\t")
    idx_sample = header.index("sample")
    idx_group = header.index("group")
    idx_conditions = [i for i, item in enumerate(header) if item.startswith('condition')]
    idx_folder = header.index("folder")
    idx_pair1 = header.index("pair1")
    idx_pair2 = header.index("pair2")
    idx_unpaired = header.index("unpaired")

    # get condition names
    CONDITION_NAMES = [header[idx_cond].split("=")[1] for idx_cond in idx_conditions]

    for line in f:
        splits = line.strip("\n").split("\t")
        sample = splits[idx_sample]
        group = splits[idx_group]

        # add samples per group
        if group in GROUP_SAMPLES:
            GROUP_SAMPLES[group].append(sample)
        else:
            GROUP_SAMPLES[group] = [sample]
            GROUPS.append(group)

        # add conditions per sample
        SAMPLE_CONDITIONS[sample] = [splits[idx_cond] for idx_cond in idx_conditions]

        # collect files per sample
        SAMPLE_FOLDER[sample] = os.path.normpath(splits[idx_folder] ) + os.sep
        SAMPLE_P1[sample] = splits[idx_pair1].split(",")
        SAMPLE_P2[sample] = splits[idx_pair2].split(",")
        SAMPLE_UP[sample] = splits[idx_unpaired].split(",") if splits[idx_unpaired] else ""

        # link each file of the sample to the host filter
        if group in HOST_FILTER_REF:
            HOST_FILTER_SAMPLES.add(sample)
            for file in SAMPLE_P1[sample]:
                HOST_FILTER_FILES[file] = HOST_FILTER_REF[group]
            for file in SAMPLE_P2[sample]:
                HOST_FILTER_FILES[file] = HOST_FILTER_REF[group]
            for file in SAMPLE_UP[sample]:
                HOST_FILTER_FILES[file] = HOST_FILTER_REF[group]

# first condition pairs
MAIN_CONDITIONS = dict()
MAIN_CONDITIONS_PAIRS = dict()
for group in GROUPS:
    MAIN_CONDITIONS[group] = set()
    [MAIN_CONDITIONS[group].add(SAMPLE_CONDITIONS[sample][0]) for sample in GROUP_SAMPLES[group]]
    MAIN_CONDITIONS_PAIRS[group] = [x + "vs" + y for x in MAIN_CONDITIONS[group] for y in MAIN_CONDITIONS[group] if x != y]
