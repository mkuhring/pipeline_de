### Filter Host Reads with BMTagger ###

# prepare indices for BMTagger
rule hostfilter_bmtagger_index_bitmask:
    input:
        fasta = "{reference}_deconseq/deconseq.splits.fasta"
    output:
        bitmask = "{reference}_bmtagger/bmtagger.bitmask"
    log:
        "logs/hostfilter/bmtagger/{reference}.bmtool.log"
    benchmark:
        "logs/hostfilter/bmtagger/{reference}.bmtool.txt"
    shell:
        "bmtool -d {input.fasta} -o {output.bitmask} -A 0 -w 18 2>&1 | tee {log}"

rule hostfilter_bmtagger_index_srprism:
    input:
        fasta = "{reference}_deconseq/deconseq.splits.fasta"
    output:
        tag = "{reference}_bmtagger/bmtagger.srprism.tag"
    params:
        out = "{reference}_bmtagger/bmtagger.srprism"
    log:
        "logs/hostfilter/bmtagger/{reference}.srprism.log"
    benchmark:
        "logs/hostfilter/bmtagger/{reference}.srprism.txt"
    shell:
        "srprism mkindex -i {input.fasta} -o {params.out} -M 131072 2>&1 | tee {log} && touch {output.tag}"

# run BMTagger
rule hostfilter_bmtagger_singleend:
    input:
        reads = "{folder}/r{pair}_UP_{identifier}_R{pair}.fastq",
        bitmask = lambda wc: GENOMES[HOST_FILTER_FILES["r" + wc.pair + "_UP_" + wc.identifier + "_R" + wc.pair + ".fastq"]] + "_bmtagger/bmtagger.bitmask",
        srprism = lambda wc: GENOMES[HOST_FILTER_FILES["r" + wc.pair + "_UP_" + wc.identifier + "_R" + wc.pair + ".fastq"]] + "_bmtagger/bmtagger.srprism.tag"
    output:
        reads = "{folder}/r{pair,[12]}_UP_{identifier}_R{pair}.fastq_bmtagger/final.fastq",
        temp  = temp("{folder}/r{pair,[12]}_UP_{identifier}_R{pair}.fastq_bmtagger/temp/")
    params:
        srprism = lambda wc: GENOMES[HOST_FILTER_FILES["r" + wc.pair + "_UP_" + wc.identifier + "_R" + wc.pair + ".fastq"]] + "_bmtagger/bmtagger.srprism",
        out_prefix = "{folder}/r{pair}_UP_{identifier}_R{pair}.fastq_bmtagger/final",
    log:
        "logs/hostfilter/bmtagger/r{pair}_UP_{identifier}_R{pair}.fastq.log"
    benchmark:
        "logs/hostfilter/bmtagger/r{pair}_UP_{identifier}_R{pair}.fastq.txt"
    shell:
        "bmtagger.sh -b {input.bitmask} -x {params.srprism} -T {output.temp} -q1 -1 {input.reads} -o {params.out_prefix} -X 2>&1 | tee {log}"

rule hostfilter_bmtagger_pairedend:
    input:
        reads_p1 = "{folder}/r1_P_{identifier}_R1.fastq",
        reads_p2 = "{folder}/r2_P_{identifier}_R2.fastq",
        bitmask = lambda wc: GENOMES[HOST_FILTER_FILES["r1_P_" + wc.identifier + "_R1.fastq"]] + "_bmtagger/bmtagger.bitmask",
        srprism = lambda wc: GENOMES[HOST_FILTER_FILES["r1_P_" + wc.identifier + "_R1.fastq"]] + "_bmtagger/bmtagger.srprism.tag"
    output:
        reads_p1 = "{folder}/r1_P_{identifier}_R1.fastq_bmtagger/final_1.fastq",
        reads_p2 = "{folder}/r1_P_{identifier}_R1.fastq_bmtagger/final_2.fastq",
        temp     = temp("{folder}/r1_P_{identifier}_R1.fastq_bmtagger/temp/")
    params:
        srprism = lambda wc: GENOMES[HOST_FILTER_FILES["r1_P_" + wc.identifier + "_R1.fastq"]] + "_bmtagger/bmtagger.srprism",
        out_prefix = "{folder}/r1_P_{identifier}_R1.fastq_bmtagger/final"
    log:
        "logs/hostfilter/bmtagger/r1_P_{identifier}_R1.fastq.log"
    benchmark:
        "logs/hostfilter/bmtagger/r1_P_{identifier}_R1.fastq.txt"
    shell:
        "bmtagger.sh -b {input.bitmask} -x {params.srprism} -T {output.temp} -q1 -1 {input.reads_p1} -2 {input.reads_p2} -o {params.out_prefix} -X 2>&1 | tee {log}"

rule hostfilter_bmtagger_move_pairedend:
    input:
        reads_p1 = "{folder}/r1_P_{identifier}_R1.fastq_bmtagger/final_1.fastq",
        reads_p2 = "{folder}/r1_P_{identifier}_R1.fastq_bmtagger/final_2.fastq"
    output:
        reads_p1 = "{folder}/r1_P_{identifier}_R1.fastq_bmtagger/final.fastq",
        reads_p2 = "{folder}/r2_P_{identifier}_R2.fastq_bmtagger/final.fastq"
    shell:
        '''
        mv {input.reads_p1} {output.reads_p1}
        mv {input.reads_p2} {output.reads_p2}
        '''
