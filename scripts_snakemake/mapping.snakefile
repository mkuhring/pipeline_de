### Read Mapping with TopHat ###

# Bowtie2 indexing of the genome
rule bowtie2_indexing:
    version:
        subprocess.check_output("bowtie2-build --version | head -1 | cut -d \" \" -f3", shell=True)
    input:
        "{genome}"
    output:
        expand("{{genome}}.{index}.bt2", index=range(1,4)),
        expand("{{genome}}.rev.{index}.bt2", index=range(1,2))
    params:
        bt2_index_base="{genome}"
    threads:
        THREADS_MAX
    benchmark:
        "logs/bowtie2-build/{genome}.txt"
    log:
        "logs/bowtie2-build/{genome}.log"
    shell:
        "bowtie2-build --threads {threads} {input} {params.bt2_index_base} 2>&1 | tee {log}"

# group/concatenate files of a sample for TopHat input
# TODO: rewrite to consider only unpaired, if no pairs are available
def groupSampleFilesForTophat(wc):
    if wc.sample in HOST_FILTER_SAMPLES:
        pair1 = ",".join(SAMPLE_FOLDER[wc.sample] + file + "_" + HOST_FILTER_METHOD + "/final.fastq" for file in SAMPLE_P1[wc.sample])
        pair2 = ",".join(SAMPLE_FOLDER[wc.sample] + file + "_" + HOST_FILTER_METHOD + "/final.fastq" for file in SAMPLE_P2[wc.sample])
        unpaired = "," + ",".join(SAMPLE_FOLDER[wc.sample] + file + "_" + HOST_FILTER_METHOD + "/final.fastq" for file in SAMPLE_UP[wc.sample])
    else:
        pair1 = ",".join(SAMPLE_FOLDER[wc.sample] + file for file in SAMPLE_P1[wc.sample])
        pair2 = ",".join(SAMPLE_FOLDER[wc.sample] + file for file in SAMPLE_P2[wc.sample])
        unpaired = "," + ",".join(SAMPLE_FOLDER[wc.sample] + file for file in SAMPLE_UP[wc.sample]) \
            if SAMPLE_UP[wc.sample] else ""
    return pair1 + " " + pair2 + unpaired

# execute TopHat
# (expects paired-end plus single-end, based on QCumber or Host filter output)
rule tophat_pe:
    version:
        subprocess.check_output("tophat --version | cut -d \" \" -f2", shell=True)
    input:
        pair1 = lambda wc: expand("{folder}{file}_" + HOST_FILTER_METHOD + "/final.fastq", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_P1[wc.sample])\
            if wc.sample in HOST_FILTER_SAMPLES else expand("{folder}{file}", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_P1[wc.sample]),
        pair2 = lambda wc: expand("{folder}{file}_" + HOST_FILTER_METHOD + "/final.fastq", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_P2[wc.sample])\
            if wc.sample in HOST_FILTER_SAMPLES else expand("{folder}{file}", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_P2[wc.sample]),
        up = lambda wc: expand("{folder}{file}_" + HOST_FILTER_METHOD + "/final.fastq", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_UP[wc.sample])\
            if wc.sample in HOST_FILTER_SAMPLES else expand("{folder}{file}", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_UP[wc.sample]),
        anno = lambda wc: ANNOTATIONS[wc.reference],
        btf1 = lambda wc: GENOMES[wc.reference] + ".1.bt2",
        btf2 = lambda wc: GENOMES[wc.reference] + ".rev.1.bt2",
        fa = lambda wc: GENOMES[wc.reference]
    output:
        "tophat/{reference}_vs_{sample}/align_summary.txt",
        "tophat/{reference}_vs_{sample}/accepted_hits.bam"
    params:
        fastq = groupSampleFilesForTophat,
        bt2_index_base = lambda wc: GENOMES[wc.reference],
        outdir = "tophat/{reference}_vs_{sample}",
        nnj = "--no-novel-juncs" if REF_ONLY else ""
    threads:
        THREADS_MAX
    benchmark:
        "logs/tophat/{reference}_vs_{sample}.txt"
    log:
        "logs/tophat/{reference}_vs_{sample}.log"
    shell:
        "tophat -p {threads} -G {input.anno} {params.nnj} -o {params.outdir} {params.bt2_index_base} {params.fastq} 2>&1 | tee {log}"


### Transcript assembly with Cufflinks and Cuffmerge ###

# execute cufflinks to assemble TopHat mappings to transcripts
rule cufflinks:
    version:
        subprocess.check_output("cufflinks 2>&1 >/dev/null | head -1 | cut -d \" \" -f2", shell=True)
    input:
        bam = "tophat/{reference}_vs_{sample}/accepted_hits.bam",
        gtf = lambda wc: ANNOTATIONS_GTF[wc.reference]
    output:
        "cufflinks/{reference}_vs_{sample}/transcripts.gtf"
    params:
        outdir = "cufflinks/{reference}_vs_{sample}/",
        gtf = lambda wc: "--GTF " + ANNOTATIONS_GTF[wc.reference] if REF_ONLY else "--GTF-guide " + ANNOTATIONS_GTF[wc.reference]
    threads:
        THREADS_MAX
    benchmark:
        "logs/cufflinks/{reference}_vs_{sample}.txt"
    log:
        "logs/cufflinks/{reference}_vs_{sample}.log"
    shell:
        "cufflinks -p {threads} {params.gtf} -o {params.outdir} {input.bam} 2>&1 | tee {log}"

# collect transcripts of same group for cuffmerge
rule cuffmerge_prep:
    input:
        lambda wc: ["cufflinks/" + wc.reference + "_vs_" + sample + "/transcripts.gtf" for sample in listGroupSamples(wc.group)]
    output:
        "cuffmerge/{group}/{reference}/assemblies.txt"
    run:
        f = open(output[0],'w')
        for i, gtf in enumerate(input):
            f.write(gtf + "\n")
        f.close()

# execute cuffmerge to get a united transcriptome
# (i.e. for all samples/replicates of a group/timeseries)
rule cuffmerge:
    input:
        # shadowing needs os.path.abspath for relative file paths outside the working directory
        # TODO: make all input file paths absolute directly in configuration?
        fasta = lambda wc: os.path.abspath(GENOMES[wc.reference]),
        gtf = lambda wc: os.path.abspath(ANNOTATIONS_GTF[wc.reference]),
        assemblies = "cuffmerge/{group}/{reference}/assemblies.txt"
    output:
        "cuffmerge/{group}/{reference}/merged.gtf"
    shadow:
        # shadow, because several instances of cuffmerge use the same files:
        # https://github.com/cole-trapnell-lab/cufflinks/issues/33
        "shallow"
    params:
        outdir="cuffmerge/{group}/{reference}"
    threads:
        THREADS_MAX
    benchmark:
        "logs/cuffmerge/{group}/{reference}.txt"
    log:
        "logs/cuffmerge/{group}/{reference}.log"
    shell:
        "cuffmerge -p {threads} -o {params.outdir} -s {input.fasta} -g {input.gtf} {input.assemblies} 2>&1 | tee {log}"

# modify gene_ids in Cuffmerge merged GTF
# i.e. replace gene_id by reference gene_id, if available
rule cuffmerge_idfix:
    input:
        ids = lambda wc: ANNOTATIONS_GTF[wc.reference] + ".ids",
        gtf = "cuffmerge/{group}/{reference}/merged.gtf"
    output:
        "cuffmerge/{group}/{reference}/merged_fixed.gtf"
    script:
        "../scripts_python/cuffmerge_idfix.py"

# extract transcript_id, gene_id and gene_name (if available) from final cuffmerge GTF file
rule cuffmerge_id_extraction:
    input:
        "cuffmerge/{group}/{reference}/merged_fixed.gtf"
    output:
        "cuffmerge/{group}/{reference}/merged_fixed.gtf.ids"
    shell:
        "{WORKFLOW_PATH}/scripts_python/gtfIDExtraction.py < {input} > {output}"

# extract potentially new gene annotations
# i.e. every transcript which has an Cuffcompare class_code "u"
rule cuffmerge_extract_novels:
    input:
        "cuffmerge/{group}/{reference}/merged_fixed.gtf"
    output:
        "cuffmerge/{group}/{reference}/novel_trans.gtf"
    shell:
        "grep \"class_code \\\"u\\\"\" {input} > {output}"

# extract potentially new transcript sequences
rule cuffmerge_extract_sequences:
    input:
        gtf = "cuffmerge/{group}/{reference}/novel_trans.gtf",
        fasta = lambda wc: GENOMES[wc.reference]
    output:
        fasta = "cuffmerge/{group}/{reference}/novel_trans.fasta"
    shell:
        "gffread {input.gtf} -g {input.fasta} -w {output.fasta}"
