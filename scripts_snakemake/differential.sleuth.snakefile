### Differential Expression with Sleuth ###

# collect kallisto results for a group and indicate sample/replicate, condition and path
rule kallisto_file_summary:
    input:
        h5 = lambda wc: ["kallisto/" + wc.group + "/" + wc.reference + "_vs_" + sample + "/abundance.h5" for sample in listGroupSamples(wc.group)]
    output:
        "kallisto/{group}/{reference}/de-{mode}/files.txt"
    run:
        wc = wildcards
        f = open(output[0],'w')
        f.write("sample" + "\t" + "\t".join(CONDITION_NAMES) + "\t" + "path" + "\n")
        for sample in GROUP_SAMPLES[wc.group]:
            path = "kallisto/" + wc.group + "/" + wc.reference + "_vs_" + sample
            cond = "\t".join(SAMPLE_CONDITIONS[sample])
            line = sample + "\t" + cond + "\t" + path + "\n"
            f.write(line)
        f.close()

# run sleuth
rule sleuth_timeseries:
    input:
        samples = "kallisto/{group}/{reference}/de-timeseries/files.txt",
        ids = lambda wc: ANNOTATIONS_GTF[wc.reference] + ".ids" if wc.feature == "genes" else []
    output:
        results_compl = "sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}/results_compl.txt",
        results_sign = "sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}/results_sign.txt",
        qqplot  = "sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}/qqplot.png"
    params:
        heatmap = "sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}/heatmap.png",
        tcplot  = "sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}/tcplot.png",
        wfpath = lambda wc: WORKFLOW_PATH,
        debug = DEBUGGING,
        batch = lambda wc: BATCH_VARS[wc.design],
        level = "{level}",
        time  = TIME_SCALE,
        feature = "{feature}"
    benchmark:
        "logs/sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}.txt"
    log:
        "logs/sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}.log"
    script:
        "../scripts_r/differential.sleuth.timeseries.R"

rule sleuth_pairwise:
    input:
        samples = "kallisto/{group}/{reference}/de-pairwise/files.txt",
        ids = lambda wc: ANNOTATIONS_GTF[wc.reference] + ".ids" if wc.feature == "genes" else []
    output:
        scatter = "sleuth_{feature}/{level}/{group}/{reference}/de-pairwise/{design}/scatterMatrix.png",
        qqplot  = "sleuth_{feature}/{level}/{group}/{reference}/de-pairwise/{design}/qqplotMatrix.png"
    params:
        wfpath = lambda wc: WORKFLOW_PATH,
        debug = DEBUGGING,
        outdir = "sleuth_{feature}/{level}/{group}/{reference}/de-pairwise/{design}/",
        batch = lambda wc: BATCH_VARS[wc.design],
        level = "{level}",
        feature = "{feature}"
    benchmark:
        "logs/sleuth_{feature}/{level}/{group}/{reference}/de-pairwise/{design}.txt"
    log:
        "logs/sleuth_{feature}/{level}/{group}/{reference}/de-pairwise/{design}.log"
    script:
        "../scripts_r/differential.sleuth.pairwise.R"
