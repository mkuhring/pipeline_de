### Mapping quality reports with QualiMap ###

# general mapping quality measures via QualiMap bamqc
rule qualimap_bamqc:
    input:
        bam = "tophat/{reference}_vs_{sample}/accepted_hits.bam",
        gtf = lambda wc: ANNOTATIONS_GTF[wc.reference] \
            if REF_ONLY else "cuffmerge/" +  wc.group + "/" + wc.reference + "/merged_fixed.gtf"
    output:
        "qualimap/{group}/{reference}_vs_{sample}/bamqc_report.pdf"
    params:
        outdir  = "-outdir qualimap/{group}/{reference}_vs_{sample}/",
        outfile = "-outfile bamqc_report." + QUALIMAP_REPORT,
        strand  = "--sequencing-protocol " + QUALIMAP_PROTOCOL if QUALIMAP_PROTOCOL else ""
    log:
        "logs/qualimap_bamqc/{group}/{reference}_vs_{sample}.log"
    benchmark:
        "logs/qualimap_bamqc/{group}/{reference}_vs_{sample}.txt"
    threads:
        THREADS_MAX
    shell:
        """
        export JAVA_OPTS="-Djava.awt.headless=true -Xmx{QUALIMAP_MEMORY} -XX:MaxPermSize=1024m"
        qualimap bamqc -bam {input.bam} -gff {input.gtf} {params.strand} {params.outdir} {params.outfile} 2>&1 | tee {log}
        unset JAVA_OPTS
        """

# RNA-Seq specific quality measures via QualiMap rnaseq
rule qualimap_rnaseq:
    input:
        bam = "tophat/{reference}_vs_{sample}/accepted_hits.bam",
        gtf = lambda wc: ANNOTATIONS_GTF[wc.reference] \
            if REF_ONLY else "cuffmerge/" + wc.group + "/" + wc.reference + "/merged_fixed.gtf"
    output:
        report = "qualimap/{group}/{reference}_vs_{sample}/rnaseq_report.pdf",
        counts = "qualimap/{group}/{reference}_vs_{sample}/counts.txt"
    params:
        outdir  = "-outdir qualimap/{group}/{reference}_vs_{sample}/",
        outfile = "-outfile rnaseq_report." + QUALIMAP_REPORT,
        strand  = "--sequencing-protocol " + QUALIMAP_PROTOCOL if QUALIMAP_PROTOCOL else "",
    log: # logging turned of due to extreme log size
        "logs/qualimap_rnaseq/{group}/{reference}_vs_{sample}.log"
    benchmark:
        "logs/qualimap_rnaseq/{group}/{reference}_vs_{sample}.txt"
    shell:
        """
        export JAVA_OPTS="-Djava.awt.headless=true -Xmx{QUALIMAP_MEMORY} -XX:MaxPermSize=1024m"
        qualimap rnaseq --paired --sorted -bam {input.bam} -gtf {input.gtf} {params.strand} -oc {output.counts} {params.outdir} {params.outfile} 2>&1 | tee {log}
        unset JAVA_OPTS
        """

# calculate number of unique variants/attributes of first condition
def numberOfUniquesInFirstCondition(group):
    condition1_per_sample = [SAMPLE_CONDITIONS[sample][0] for sample in GROUP_SAMPLES[group]]
    num_of_variants = len(set(condition1_per_sample))
    return num_of_variants

# collect read counts for each group for rule qualimap_counts
rule qualimap_collect_counts:
    input:
        lambda wc: ["qualimap/" + wc.group + "/" + wc.reference +
                    "_vs_" + sample + "/counts.txt" for sample in listGroupSamples(wc.group)]
    output:
        "qualimap/{group}/{reference}/count_data.txt"
    run:
        wc = wildcards
        f = open(output[0],'w')
        for sample in GROUP_SAMPLES[wc.group]:
            file = "qualimap/" + wc.group + "/" +  wc.reference + "_vs_" + sample + "/counts.txt"
            condition = (wc.group if numberOfUniquesInFirstCondition(wc.group) > 2 else SAMPLE_CONDITIONS[sample][0])
            f.write(sample + "\t" + condition + "\t" + file + "\t" + "2" + "\n")
            # Note, qualimap counts seems to only handle two conditions so far.
            # Thus, condition is replaced by the group name if there are more conditions.
        f.close()

# read count specific quality measures via QualiMap counts
rule qualimap_counts:
    input:
        "qualimap/{group}/{reference}/count_data.txt"
    output:
        "qualimap/{group}/{reference}/count_report.{QUALIMAP_REPORT}"
    params:
        outdir  = "-outdir qualimap/{group}/{reference}",
        outfile = "-outfile count_report." + QUALIMAP_REPORT,
        # qualimap counts can compare  conditions but only two so far!
        compare = lambda wc: "--compare" if numberOfUniquesInFirstCondition(wc.group) == 2 else ""
    log:
        "logs/qualimap_counts/{group}/{reference}.log"
    benchmark:
        "logs/qualimap_counts/{group}/{reference}.txt"
    shell:
        """
        export JAVA_OPTS="-Djava.awt.headless=true -Xmx{QUALIMAP_MEMORY} -XX:MaxPermSize=1024m"
        qualimap counts {params.compare} -d {input} {params.outdir} {params.outfile} 2>&1 | tee {log}
        unset JAVA_OPTS
        """
