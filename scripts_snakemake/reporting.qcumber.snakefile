### QCumber Report Extraction ###

# so far, QCumber reports have to be added manually to the qcumber folder

# convert report pdfs to text
# (may need to install pdftotext first)
rule qcumber_pdftotext:
    input:
        "qcumber/summary_{sample}.pdf"
    output:
        "qcumber/summary_{sample}.txt"
    shell:
        "pdftotext {input}"

# relevant lines in text file
LINE_SAMPLE = 11 - 1
LINE_READ   = 45 - 1
LINE_KRAKEN = 47 - 1

# extract sample names of all available reports
QC_SAMPLES, = glob_wildcards("qcumber/summary_{sample}.pdf")

# collect relevant numbers from all available reports
rule qcumber_extraction:
    input:
        expand("qcumber/summary_{sample}.txt", sample=QC_SAMPLES)
    output:
        "qcumber/summary.txt"
    run:
        file_out = open(output[0], 'w')
        header = "sample" + "\t" + "total" + "\t" + "trimmed" + "\t" + "tpart" + "\t" + "classified" + "\t" + "cpart"  + "\n"
        file_out.write(header)
        print(header)
        for file_name in input:
            file = open(file_name)
            lines = file.readlines()

            line_out = lines[LINE_SAMPLE].strip()
            splits = lines[LINE_READ].strip().split(' ')
            line_out = line_out + "\t" + splits[2] + "\t" + splits[0] + "\t" + splits[3].strip("()")
            splits = lines[LINE_READ].strip().split(' ')
            line_out = line_out + "\t" + splits[0] + "\t" + splits[3].strip("()") + "\n"
            file_out.write(line_out)
            print(line_out)
        file_out.close()