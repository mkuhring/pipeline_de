### Result Collection Rules ###

# collect DESeq2 results (gene counts)
rule collect_deseq2_gc_results:
    input:
        [expand("deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/pca_samples.png", group=group,
                reference=REFERENCES[group], design=DESIGNS, level=DE_TEST) for group in GROUPS] \
            if "pairwise" in DE_MODE else [],
        [expand("deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/results_sig_{pair}_with_fc.csv",
                group=group, reference=REFERENCES[group], design=DESIGNS, level=DE_TEST,
                pair=MAIN_CONDITIONS_PAIRS[group]) for group in GROUPS] if "pairwise" in DE_MODE else [],
        [expand("deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/pca_samples.png", group=group,
                reference=REFERENCES[group], design=DESIGNS, level=DE_TEST) for group in GROUPS] \
            if "timeseries" in DE_MODE else [],
        [expand("deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/results_sig_ts_with_fc.csv",
                group=group, reference=REFERENCES[group], design=DESIGNS, level=DE_TEST) for group in GROUPS] \
            if "timeseries" in DE_MODE else []
    output:
        temp("results/deseq2.gc.tag")
    shell:
        "touch {output}"

# collect DESeq2 results (transcript counts)
rule collect_deseq2_tc_results:
    input:
        [expand("deseq2/{level}/kallisto/{group}/{reference}/de-pairwise/{design}/pca_samples.png", group=group,
                reference=REFERENCES[group], design=DESIGNS, level=DE_TEST) for group in GROUPS] \
            if "pairwise" in DE_MODE else [],
        [expand("deseq2/{level}/kallisto/{group}/{reference}/de-timeseries/{design}/pca_samples.png", group=group,
                reference=REFERENCES[group], design=DESIGNS, level=DE_TEST) for group in GROUPS] \
            if "timeseries" in DE_MODE else []
    output:
        temp("results/deseq2.tc.tag")
    shell:
        "touch {output}"
