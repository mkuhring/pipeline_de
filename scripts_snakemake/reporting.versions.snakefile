### Collect Software & Package Version ###

# dictionary of software and their version parameter call
# (incl. cutting, trimming, cleaning etc. if necessary)
dict_software_version = {
    "BMTagger" : "which bmtagger.sh >/dev/null && bmtagger.sh -V 2>&1 | grep 'version' | cut -d ' ' -f2",
    "Bowtie2 (bowtie2-build)" : "which bowtie2-build >/dev/null && bowtie2-build --version | head -1 | cut -d \" \" -f3",
    "Cuffdiff" : "which cuffdiff >/dev/null && cuffdiff 2>&1 >/dev/null | head -1 | cut -d \" \" -f2-3",
    "Cufflinks" : "which cufflinks >/dev/null && cufflinks 2>&1 >/dev/null | head -1 | cut -d \" \" -f2",
    "DeconSeq" : "which deconseq.pl >/dev/null && deconseq.pl -version | cut -d ' ' -f3",
    "featureCounts" : "which featureCounts >/dev/null && featureCounts -v 2>&1 >/dev/null | head -2 | tail -1 | cut -d \" \" -f2",
    "GNU R" : "which R >/dev/null && R --version | head -n1 | cut -d ' ' -f3",
    "Kallisto" : "which kallisto >/dev/null && kallisto version | cut -d ' ' -f3",
    "Python2" : "which python2 >/dev/null && python2 --version 2>&1 >/dev/null | grep -Po \"(?<=Python )[\d\.]+\"",
    "Python3" : "which python3 >/dev/null && python3 --version | grep -Po \"(?<=Python )[\d\.]+\"",
    "QualiMap": "which qualimap >/dev/null && qualimap rnaseq 2>&1 | grep 'QualiMap v.' | cut -d ' ' -f2",
    "Snakemake" : "which snakemake >/dev/null && snakemake --version",
    "Tophat" : "which tophat >/dev/null && tophat --version | cut -d \" \" -f2"
}

# list of all R packages which version should be reported
list_R_packages = [
    "cummeRbund",
    "DESeq2",
    "devtools",
    "ggdendro",
    "ggplot2",
    "grid",
    "gridExtra",
    "limma",
    "pheatmap",
    "RColorBrewer",
    "readr",
    "rhdf5",
    "sleuth",
    "splines",
    "tximport",
    "xlsx"
]

def software_version(call):
    # execute software version call on shell
    if not call:
        return "not available"
    try:
        return str(subprocess.check_output(call, shell=True, stderr=subprocess.STDOUT, universal_newlines=True)).rstrip()
    except subprocess.CalledProcessError as e:
        return "not available"

# get version of software
rule collect_versions_software:
    output:
        ver = temp("reports/versions.software.txt")
    run:
        with open(output.ver, 'w') as f_out:
            [f_out.write(s + ":\t" + software_version(dict_software_version[s]) + "\n")
             for s in sorted(dict_software_version.keys())]

# get version R packages
rule collect_versions_R_packages:
    output:
        ver = temp("reports/versions.R-packages.txt")
    run:
        R(r'''
        packages <- strsplit("{list_R_packages}", " ")[[1]]
        lines <- vector(mode = "character", length = length(packages))
        for (i in 1:length(packages)){{
            if (require(packages[i], character.only = TRUE)){{
              version <- as.character(packageVersion(packages[i]))
            }} else {{
              version <- "not available"
            }}
            lines[i] <- paste0(packages[i], ":\t", version)
        }}
        writeLines(lines, "{output.ver}")
        ''')

# merge software and R package version
rule collect_versions_all:
    input:
        s = "reports/versions.software.txt",
        r = "reports/versions.R-packages.txt"
    output:
        ver = "reports/" + EXEC_TIME + ".versions.txt"
    shell:
        '''
        echo "Software:" > {output}
        cat {input.s} >> {output}
        echo "\nR packages:" >> {output}
        cat {input.r} >> {output}
        '''
