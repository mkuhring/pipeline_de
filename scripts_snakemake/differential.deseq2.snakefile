### Differential Expression with DESeq2 ###

# run deseq2 on gene counts (via featureCounts only so far)
rule deseq2_gc_pairwise:
    input:
        counts = "counting/featureCounts/{group}/{reference}/counts.clean.txt",
        descr  = "infos/{group}/{reference}/de-pairwise/conditions.txt"
    output:
        dds = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/dds.RData",
        time_pairs = expand("deseq2/{{level}}/featureCounts/{{group}}/{{reference}}/de-pairwise/{{design}}/results_all_{pair}.csv",
                       pair=TIME_PAIRS) if "timeseries" in DE_MODE else []
    params:
        wfpath = lambda wc: WORKFLOW_PATH,
        debug = DEBUGGING,
        level = "{level}",
        batch = lambda wc: BATCH_VARS[wc.design],
        top = DE_TOP,
        outdir = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}"
    benchmark:
        "logs/deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}.txt"
    log:
        "logs/deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}.log"
    script:
        "../scripts_r/differential.deseq2.pairwise.R"

rule deseq2_gc_pairwise_vis:
    input:
        dds = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/dds.RData",
    output:
        scatter = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/pca_samples.png"
    params:
        wfpath = lambda wc: WORKFLOW_PATH,
        debug = DEBUGGING,
        level = "{level}",
        batch = lambda wc: BATCH_VARS[wc.design],
        top = DE_TOP,
        outdir = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}"
    benchmark:
        "logs/deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}_vis.txt"
    log:
        "logs/deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}_vis.log"
    script:
        "../scripts_r/differential.deseq2.pairwise.vis.R"

rule deseq2_gc_pairwise_foldchanges:
    input:
        dds = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/dds.RData"
    output:
        normal = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/results_sig_{pair}_with_fc.csv",
        filter = expand("deseq2/{{level}}/featureCounts/{{group}}/{{reference}}/de-pairwise/{{design}}/results_sig_{{pair}}_with_fc{threshold}.csv",
                        threshold = FC_THRESHOLDS)
    params:
        pair = "deseq2/{level}/featureCounts/{group}/{reference}/de-pairwise/{design}/results_sig_{pair}.csv",
        fc_thresh = FC_THRESHOLDS
    script:
        "../scripts_r/differential.deseq2.pairwise.foldchanges.R"

rule deseq2_gc_timeseries:
    input:
        counts = "counting/featureCounts/{group}/{reference}/counts.clean.txt",
        descr  = "infos/{group}/{reference}/de-timeseries/conditions.txt"
    output:
        dds = "deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/dds.RData",
        sigs = "deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/results_sig_ts.csv",
        all = "deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/results_all_ts.csv",
        scatter = "deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/pca_samples.png"
    params:
        wfpath = lambda wc: WORKFLOW_PATH,
        debug = DEBUGGING,
        level = "{level}",
        batch = lambda wc: BATCH_VARS[wc.design],
        top = DE_TOP,
        time  = TIME_SCALE,
        outdir = "deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}"
    benchmark:
        "logs/deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}.txt"
    log:
        "logs/deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}.log"
    script:
        "../scripts_r/differential.deseq2.timeseries.R"

# get fold-changes for single timeseries from pairwise mode
rule deseq2_gc_timeseries_single_foldchanges:
    input:
        time = "deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/results_sig_ts.csv",
        pairs = expand("deseq2/{{level}}/featureCounts/{{group}}/{{reference}}/de-pairwise/{{design}}/results_all_{pair}.csv", pair=TIME_PAIRS)
    output:
        normal = "deseq2/{level}/featureCounts/{group}/{reference}/de-timeseries/{design}/results_sig_ts_with_fc.csv",
        filter = expand("deseq2/{{level}}/featureCounts/{{group}}/{{reference}}/de-timeseries/{{design}}/results_sig_ts_with_fc{threshold}.csv",
                        threshold = FC_THRESHOLDS)
    params:
        fc_thresh = FC_THRESHOLDS
    script:
        "../scripts_r/differential.deseq2.timeseries.foldchanges.R"

# run deseq2 on transcript abundances (via kallisto only so far)
rule deseq2_ta_pairwise:
    input:
        kall = "kallisto/{group}/{reference}/de-pairwise/files.txt",
        ids = lambda wc: ANNOTATIONS_GTF[wc.reference] + ".ids"
    output:
        "deseq2/{level}/kallisto/{group}/{reference}/de-pairwise/{design}/pca_samples.png"
    params:
        wfpath = lambda wc: WORKFLOW_PATH,
        debug = DEBUGGING,
        level = "{level}",
        batch = lambda wc: BATCH_VARS[wc.design],
        top = DE_TOP,
        outdir = "deseq2/{level}/kallisto/{group}/{reference}/de-pairwise/{design}"
    benchmark:
        "logs/deseq2/{level}/kallisto/{group}/{reference}/de-pairwise/{design}.txt"
    log:
        "logs/deseq2/{level}/kallisto/{group}/{reference}/de-pairwise/{design}.log"
    script:
        "../scripts_r/differential.deseq2.pairwise.R"

rule deseq2_ta_timeseries:
    input:
        kall = "kallisto/{group}/{reference}/de-timeseries/files.txt",
        ids = lambda wc: ANNOTATIONS_GTF[wc.reference] + ".ids"
    output:
        "deseq2/{level}/kallisto/{group}/{reference}/de-timeseries/{design}/pca_samples.png"
    params:
        wfpath = lambda wc: WORKFLOW_PATH,
        debug = DEBUGGING,
        level = "{level}",
        batch = lambda wc: BATCH_VARS[wc.design],
        top = DE_TOP,
        time  = TIME_SCALE,
        outdir = "deseq2/{level}/kallisto/{group}/{reference}/de-timeseries/{design}"
    benchmark:
        "logs/deseq2/{level}/kallisto/{group}/{reference}/de-timeseries/{design}.txt"
    log:
        "logs/deseq2/{level}/kallisto/{group}/{reference}/de-timeseries/{design}.log"
    script:
        "../scripts_r/differential.deseq2.timeseries.R"
