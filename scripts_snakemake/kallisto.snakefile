### Kallisto pseudo-mapping and read counting ###

# clean fasta header for kallisto
# not yet used, see rule
rule kallisto_fasta_clean:
    input:
        trans = "{transcripts}"
    output:
        trans = "{transcripts}.clean"
    run:
        wc = wildcards
        f = open(output[0],'w')
        for line in open(input[0], 'r'):
            if line.startswith(">"):
                f.write(line.split("|")[0] + "\n")
            else:
                f.write(line)
        f.close()

# build index from reference transcriptome
rule kallisto_indexing:
    version:
        subprocess.check_output("kallisto version", shell=True)
    input:
        trans = "{transcripts}.clean"
    output:
        index = "{transcripts}.kallisto"
    log:
        "logs/kallisto_index/{transcripts}.log"
    benchmark:
        "logs/kallisto_index/{transcripts}.txt"
    shell:
        "kallisto index -i {output.index} {input.trans} 2>&1 | tee {log}"

# build index from reference transcriptome + novel transcripts from Tophat+Cufflinks+Cuffmerge
rule kallisto_indexing_novel:
    version:
        subprocess.check_output("kallisto version", shell=True)
    input:
        trans_ref = lambda wc: TRANSCRIPTS[wc.reference] + ".clean",
        trans_nov = "cuffmerge/{group}_FRAC/{reference}/novel_trans.fasta"
    output:
        index = "kallisto/{group}/{reference}/reference_and_novel_trans.kallisto"
    log:
        "logs/kallisto_index/{group}/{reference}/reference_and_novel_trans.log"
    benchmark:
        "logs/kallisto_index/{group}/{reference}/reference_and_novel_trans.txt"
    shell:
        "kallisto index -i {output.index} {input} 2>&1 | tee {log}"

# group/concatenate files of a sample for Kallisto input
def groupSampleFilesForKallisto(wc):
    pair1 = [SAMPLE_FOLDER[wc.sample] + file for file in SAMPLE_P1[wc.sample]]
    pair2 = [SAMPLE_FOLDER[wc.sample] + file for file in SAMPLE_P2[wc.sample]]
    return " ".join([" ".join(pair) for pair in list(zip(pair1, pair2))])

# pseudo-map reads to transcriptome and count it
rule kallisto_quantification:
    version:
        subprocess.check_output("kallisto version", shell=True)
    input:
        pair1 = lambda wc: expand("{folder}{file}", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_P1[wc.sample]),
        pair2 = lambda wc: expand("{folder}{file}", folder=SAMPLE_FOLDER[wc.sample], file=SAMPLE_P2[wc.sample]),
        # reads1 = "reads/{reads}_1.fastq",
        # reads2 = "reads/{reads}_2.fastq",
        index = lambda wc: "kallisto/" + wc.group + "/" + wc.reference + "/reference_and_novel_trans.kallisto" \
                            if KALLISTO_NOVEL else TRANSCRIPTS[wc.reference] + ".kallisto"
    output:
        "kallisto/{group}/{reference}_vs_{sample}/abundance.h5",
        "kallisto/{group}/{reference}_vs_{sample}/abundance.tsv",
        "kallisto/{group}/{reference}_vs_{sample}/run_info.json"
    params:
        fastq = groupSampleFilesForKallisto,
        outdir = "kallisto/{group}/{reference}_vs_{sample}",
        bootstrap = "-b " + str(KALLISTO_BOOTSTRAPS) if KALLISTO_BOOTSTRAPS > 0 else ""
    log:
        "logs/kallisto_quant/{group}/{reference}_vs_{sample}.log"
    benchmark:
        "logs/kallisto_quant/{group}/{reference}_vs_{sample}.txt"
    threads:
        THREADS_MAX
    shell:
        "kallisto quant -t {threads} {params.bootstrap} -i {input.index} -o {params.outdir} {params.fastq} 2>&1 | tee {log}"

# count reads in fastq file
rule read_count:
    input:
        "reads/{sample}.fastq"
    output:
        "reads/{sample}.count"
    shell:
        "echo $((`wc -l < {input}`/4)) > {output}"


### Saturation analysis via Kallisto counts and TPMs ###

# saturation analysis via R script
rule kallisto_saturation:
    input:
        abundance = expand("kallisto/{{group}}/{{reference}}_vs_{{sample}}{fraction}/abundance.tsv", fraction=KALLISTO_FRACTIONS),
        readcount = expand("reads/{{sample}}{fraction}_1.count", fraction=KALLISTO_FRACTIONS),
        kallisto = expand("kallisto/{{group}}/{{reference}}_vs_{{sample}}{fraction}/run_info.json", fraction=KALLISTO_FRACTIONS)
    output:
        count = "saturation/{group}/{reference}_vs_{sample}/trans-count.pdf",
        dens = "saturation/{group}/{reference}_vs_{sample}/tpm-dens.pdf",
        pre1 = "saturation/{group}/{reference}_vs_{sample}/pre_all.pdf",
        pre2 = "saturation/{group}/{reference}_vs_{sample}/pre_no-outliers.pdf"
    log:
        "logs/saturation/{group}/{reference}_vs_{sample}.log"
    script:
        "../scripts_r/saturation.kallisto.R"
