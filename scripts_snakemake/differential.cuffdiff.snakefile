### Differential Expression with Cuffdiff ###

# get file name of TopHat mapping results for a certain sample and reference
def getMappingFile(wc, sample):
    return "tophat/" + wc.reference + "_vs_" + sample + "/accepted_hits.bam"

# create Cuffdiff input string with labels and replicate list (separated by ",") per conditions (separated by " ")
# only first condition columen is used for Cuffdiff
# e.g. "--labels A,B,C A1.bam,A2.bam B1.bam,B2.bam,B3.bam C1.bam
def groupReplicates(wc):
    conditions = list()
    condition_replicates = dict()
    for sample in GROUP_SAMPLES[wc.group]:
        condition = SAMPLE_CONDITIONS[sample][0]
        if condition not in conditions:
            conditions.append(condition)
        if condition in condition_replicates:
            condition_replicates[condition].append(getMappingFile(wc, sample))
        else:
            condition_replicates[condition] = [getMappingFile(wc, sample)]
    return "--labels " + ",".join(conditions) + " " + " ".join([",".join(condition_replicates[condition]) for condition in conditions])

    # replicate_lists = [REPLICATES[condition] for condition in CONDITIONS[wc.group]]
    # return " ".join(",".join([getMappingFile(wc, sample) for sample in replicates]) for replicates in replicate_lists)


# execute Cuffdiff within groups (pairwise or timeseries)
rule cuffdiff:
    version:
        subprocess.check_output("cuffdiff 2>&1 >/dev/null | head -1 | cut -d \" \" -f2-3", shell=True)
    input:
        fa = lambda wc: GENOMES[wc.reference],
        gtf = lambda wc: ANNOTATIONS[wc.reference] \
            if REF_ONLY else "cuffmerge/" +  wc.group + "/" + wc.reference + "/merged.gtf",
        bams = lambda wc: ["tophat/" + wc.reference + "_vs_" + sample + "/accepted_hits.bam" for sample in listGroupSamples(wc.group)]
    output:
        "cuffdiff/{group}/{reference}/de-{mode}/isoform_exp.diff",
        "cuffdiff/{group}/{reference}/de-{mode}/gene_exp.diff"
    params:
        labels_and_bams    = groupReplicates, # string of comma-separated list of condition labels and lists of replicates per label/condition
        outdir  = "cuffdiff/{group}/{reference}/de-{mode}/",
        mrc     = "--multi-read-correct",  # use 'rescue method' for multi-reads
        fbr     = lambda wc: "--frag-bias-correct " + GENOMES[wc.reference],   # use bias correction - reference fasta required
        mode    = lambda wc: "--time-series" if wc.mode == "timeseries" else ""    # treat samples as a time-series
    threads:
        THREADS_MAX
    benchmark:
        "logs/cuffdiff/{group}/{reference}_{mode}.txt"
    log:
        "logs/cuffdiff/{group}/{reference}_{mode}.log"
    shell:
        "cuffdiff -p {threads} {params.mode} {params.mrc} -o {params.outdir} {params.fbr} {input.gtf} {params.labels_and_bams}"

rule cuffdiff_visualization:
    input:
        "cuffdiff/{group}/{reference}/de-{mode}/isoform_exp.diff",
        "cuffdiff/{group}/{reference}/de-{mode}/gene_exp.diff"
    output:
        "cuffdiff_vis/{group}/{reference}/de-{mode}/csScatterMatrix.png",
        "cuffdiff_vis/{group}/{reference}/de-{mode}/csVolcanoMatrix.png"
    params:
        indir = "cuffdiff/{group}/{reference}/de-{mode}/",
        outdir = "cuffdiff_vis/{group}/{reference}/de-{mode}/",
        debug = DEBUGGING
    log:
        "logs/cuffdiff_vis/{group}/{reference}/de-{mode}.log"
    script:
        "../scripts_r/visualization.cuffdiff.R"
