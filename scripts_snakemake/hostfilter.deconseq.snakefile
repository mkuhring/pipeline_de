### Filter Host Reads with DeconSeq ###

# prepare fasta files for DeconSeq
# according to: http://deconseq.sourceforge.net/manual.html
rule hostfilter_prepare_db_1_splitting:
    input:
        fasta = "{reference}"
    output:
        fasta = "{reference}_deconseq/deconseq.splits.fasta"
    shell:
        r"cat {input.fasta} | perl -p -e 's/N\n/N/' | perl -p -e 's/^N+//;s/N+$//;s/N{{200,}}/\n>split\n/' > {output.fasta}"

rule hostfilter_prepare_db_2_filtering:
    input:
        fasta = "{reference}_deconseq/deconseq.splits.fasta"
    output:
        fasta = "{reference}_deconseq/deconseq.fasta"
    params:
        out_name = "{reference}_deconseq/deconseq",
        seq_id = lambda wc: wc.reference + "_"
    log:
        "logs/hostfilter/prinseq/{reference}.log"
    benchmark:
        "logs/hostfilter/prinseq/{reference}.txt"
    shell:
        "prinseq-lite.pl -log {log} -verbose -fasta {input.fasta} -min_len 200 -ns_max_p 10 -derep 12345 " \
        "-out_good {params.out_name} -seq_id {params.seq_id} -rm_header -out_bad null"

rule hostfilter_prepare_db_3_indexing:
    input:
        fasta = "{reference}_deconseq/deconseq.fasta"
    output:
        fasta = "{reference}_deconseq/deconseq.fasta.bwt"
    log:
        "logs/hostfilter/bwa/{reference}.log"
    benchmark:
        "logs/hostfilter/bwa/{reference}.txt"
    shell:
        "bwa64 index -a bwtsw {input.fasta} 2>&1 | tee {log}"

# run DeconSeq
rule hostfilter_deconseq:
    input:
        reads = "{folder}/{file}",
        host = lambda wc: GENOMES[HOST_FILTER_FILES[wc.file]] + "_deconseq/deconseq.fasta.bwt"
    output:
        reads = "{folder}/{file}_deconseq/deconseq_clean.fq"
    params:
        dbs    = lambda wc: HOST_FILTER_FILES[wc.file],
        outdir = "{folder}/{file}_deconseq/"
    shadow: "shallow"
    log:
        "logs/hostfilter/deconseq/{file}.log"
    benchmark:
        "logs/hostfilter/deconseq/{file}.txt"
    shell:
        "deconseq.pl -f {input.reads} --dbs {params.dbs} -out_dir {params.outdir} -id deconseq 2>&1 | tee {log}"

# restructure paired-end files, since DeconSeq doesn't account for them
# i.e. move reads from P to UP if there is no matching pair anymore
rule hostfilter_restructure_paires:
    input:
        p1 = "{folder}/r1_P_{identifier}_R1.fastq_deconseq/deconseq_clean.fq",
        p2 = "{folder}/r2_P_{identifier}_R2.fastq_deconseq/deconseq_clean.fq",
        up1 = "{folder}/r1_UP_{identifier}_R1.fastq_deconseq/deconseq_clean.fq",
        up2 = "{folder}/r2_UP_{identifier}_R2.fastq_deconseq/deconseq_clean.fq",
    output:
        p1 = "{folder}/r1_P_{identifier}_R1.fastq_deconseq/final.fastq",
        p2 = "{folder}/r2_P_{identifier}_R2.fastq_deconseq/final.fastq",
        up1 = "{folder}/r1_UP_{identifier}_R1.fastq_deconseq/final.fastq",
        up2 = "{folder}/r2_UP_{identifier}_R2.fastq_deconseq/final.fastq",
    run:
        print("TODO...")

        # copy up1 and up2
        shell("cp {input.up1} {output.up1}")
        shell("cp {input.up2} {output.up2}")

        # check intersection of p1 and p2
        with open(input.p1, 'r') as p1_in:
            reads_p1 = {line[1:].split(" ")[0] for count,line in enumerate(p1_in) if count % 4 == 0}
        with open(input.p2, 'r') as p2_in:
            reads_p2 = {line[1:].split(" ")[0] for count,line in enumerate(p2_in) if count % 4 == 0}
        reads_paired = reads_p1.intersection(reads_p2)

        # iterate p1, keep intersecting reads, append single reads to up1
        with open(input.p1, 'r') as p1_in:
            with open(output.p1, 'w') as p1_out:
                with open(output.up1, 'a') as up1_out:
                    keep = True
                    for count,line in enumerate(p1_in):
                        if count % 4 == 0:
                            keep = line[1:].split(" ")[0] in reads_paired
                        p1_out.write(line) if keep else up1_out.write(line)

        # iterate p2, keep intersecting reads, append single reads to up2
        with open(input.p2, 'r') as p2_in:
            with open(output.p2, 'w') as p2_out:
                with open(output.up2, 'a') as up2_out:
                    keep = True
                    for count,line in enumerate(p2_in):
                        if count % 4 == 0:
                            keep = line[1:].split(" ")[0] in reads_paired
                        p2_out.write(line) if keep else up2_out.write(line)
