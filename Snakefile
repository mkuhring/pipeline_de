import os
import datetime
import subprocess

from itertools import chain
from snakemake.utils import R

WORKFLOW_PATH = os.path.dirname(os.path.realpath(workflow.snakefile))
EXEC_TIME = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

### Configuration ###
include: WORKFLOW_PATH + "/scripts_snakemake/configuration.snakefile"

# function to list all samples in a group
def listGroupSamples(group):
    return GROUP_SAMPLES[group]

# induce renamening gff -> gff3 (via symlink)
# induce conversion gff(3) -> gtf (qualimap_rnaseq needs gtf, qualimap_bamqc prefers gtf)
ANNOTATIONS_GTF = {}
for ref, file in ANNOTATIONS.items():
    ANNOTATIONS[ref] = re.sub('\.gff3$', '.gff', file)
    ANNOTATIONS_GTF[ref] = re.sub('\.gff3?$', '.gtf', file)

### RULES ###
include: WORKFLOW_PATH + "/scripts_snakemake/differential.cuffdiff.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/differential.deseq2.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/differential.sleuth.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/hostfilter.bmtagger.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/hostfilter.deconseq.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/kallisto.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/mapping.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/preparation.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/quality.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/quantification.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/reporting.qcumber.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/reporting.versions.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/reporting.snakefile"
include: WORKFLOW_PATH + "/scripts_snakemake/results.snakefile"


# run workflow
rule all:
    input:
        ### qualimap ###
        [expand("qualimap/{group}/{reference}_vs_{sample}/bamqc_report.pdf", reference=REFERENCES[group],
                sample=listGroupSamples(group), group=group) for group in GROUPS] if QUALIMAP_ON else [],
        [expand("qualimap/{group}/{reference}/count_report.pdf", reference=REFERENCES[group],
                group=group) for group in GROUPS] if QUALIMAP_ON else [],
        ### saturation (kallisto) ###
        [expand("saturation/{group}/{reference}_vs_{sample}/trans-count.pdf", reference=REFERENCES[group],
                sample=listGroupSamples(group), group=group) for group in GROUPS] if SATURATION_ON else [],
        ### deseg2 ###
        "results/deseq2.gc.tag" if "deseq2" in DE_METHODS else [],
        "results/deseq2.tc.tag" if "deseq2_kallisto" in DE_METHODS else [],
        ### cuffdiff ###
        [expand("cuffdiff_vis/{group}/{reference}/de-timeseries/csScatterMatrix.png",
                reference=REFERENCES[group], group=group) for group in GROUPS] \
                if "cuffdiff" in DE_METHODS and "timeseries" in DE_MODE else [],
        [expand("cuffdiff_vis/{group}/{reference}/de-pairwise/csScatterMatrix.png",
                reference=REFERENCES[group], group=group) for group in GROUPS] \
                if "cuffdiff" in DE_METHODS and "pairwise" in DE_MODE else [],
        ### sleuth ###
        [expand("sleuth_{feature}/{level}/{group}/{reference}/de-timeseries/{design}/results_sign.txt",
                reference=REFERENCES[group], group=group, design=DESIGNS, level=DE_TEST, feature=SLEUTH_FEATURE) \
                for group in GROUPS] if "sleuth" in DE_METHODS and "timeseries" in DE_MODE else [],
        [expand("sleuth_{feature}/{level}/{group}/{reference}/de-pairwise/{design}/scatterMatrix.png",
                reference=REFERENCES[group], group=group, design=DESIGNS, level=DE_TEST, feature=SLEUTH_FEATURE) \
                for group in GROUPS] if "sleuth" in DE_METHODS and "pairwise" in DE_MODE else [],
        ### novel transcripts ###
        [expand("cuffmerge/{group}/{reference}/novel_trans.fasta", reference=REFERENCES[group], group=group) for group in GROUPS],
        ### reporting ###
        "reports/" + EXEC_TIME + ".report.txt",
        "reports/" + EXEC_TIME + ".versions.txt",
        # if qcumber pdf reports are available (i.e. qcumber folder exists), summarize them
        "qcumber/summary.txt" if os.path.isdir("qcumber/") else []
